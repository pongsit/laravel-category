@props(['subcategories'=>$subcategories,'level'=>$level])

@if(!empty($subcategories))
  @foreach($subcategories->load(['subcategories'=>function($query){
      $query->where('status',1)->orderBy('power','desc');
    }]) as $v)
    <option value="{{$v->id}}">
      @for($i=1;$i<=$level;$i++) - @endfor
      {{$v->name}}
    </option>
    @if(!empty($v->subcategories))
      <x-category::modal.option
        :subcategories="$v->subcategories"
        :level="$level+1"
      />
    @endif
  @endforeach
@endif