<?php

namespace Pongsit\Category\Models;

use Pongsit\Category\Database\Factories\CategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Pongsit\User\Models\User;


class Category extends Model
{
  protected $guarded = [];

  // public function posts()
  // {
  //     return $this->morphedByMany('Pongsit\User\Models\User', 'categorizable');
  // }

  public function category()
  {
      return $this->belongsTo($this);
  }

  public function subcategories()
  {
      return $this->hasMany($this);
  }

}
