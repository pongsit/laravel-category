<?php

namespace Pongsit\Category;

use GuzzleHttp\Middleware;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Pongsit\Category\Providers\EventServiceProvider;

class CategoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->mergeConfigFrom(
        //     __DIR__.'/../config/role.php', 'services'
        // );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['router']->namespace('Pongsit\\Category\\Controllers')
                ->middleware(['web'])
                ->group(function () {
                    $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
                });
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'category');
        $this->publishes([
            __DIR__.'/../config/category.php' => config_path('category.php'),
        ], 'public');
    }
}