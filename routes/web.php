<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use Pongsit\Category\Http\Controllers\CategoryController;

Route::post('/category/store', [CategoryController::class, 'store'])->name('category.store');
Route::get('/category', [CategoryController::class, 'index'])->name('category.index');
