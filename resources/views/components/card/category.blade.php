@php $categories = \Pongsit\Category\Models\Category::where('status',1)->whereNull('category_id')->orderBy('power','desc')->get(); @endphp

@if(user()->can('manage_category'))
    <div class="card shadow">
        <div class="card-header pt-3 pb-3 d-flex justify-content-between">
            <div><h6 class="m-0 font-weight-bold">หมวดหมู่</h6></div>
            <div><a href="{{route('category.index')}}" ><i class="fas fa-edit"></i> </a></div>
        </div>
        <div class="card-body">
            @php
                $bg = '';
                $bg_mem = $bg; 
                $empty = true;
            @endphp
            @foreach($categories->load(['subcategories'=>function($query){
                    $query->where('status',1)->orderBy('power','desc');
                }]) as $v)
                <div class="d-flex justify-content-between" style="{{$bg}}">
                    <div>
                        {{$v->name}}
                    </div>
                </div>
                @php 
                    if(!empty($bg)){
                        $bg = $bg_mem;
                    }else{
                        $bg = 'background-color:rgb(33,37,41,0.6);';
                    }
                    $empty = false;
                @endphp
                @if(!empty($v->subcategories))
                    <x-category::subcategory 
                        :subcategories="$v->subcategories"
                        :menu="false"
                    />
                @endif
            @endforeach
            @if($empty)
                ยังไม่มีหมวดหมู่
            @endif
        </div>
    </div>
@endif