<?php

namespace Pongsit\Category\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;
use Pongsit\User\Models\User;
use Pongsit\Category\Models\Category;

class CategoryController extends Controller
{
	public function index(){
		$variables['categories'] = Category::where('status',1)->whereNull('category_id')->orderBy('power','desc')->get();
		return view('category::index',$variables);
	}


	public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
        ],[
            'name.required'=>'กรุณากรอกชื่อ',
        ]);

        $infos = [];
        $infos['name'] = $request->name;

        if(!empty($request->description)){
            $infos['description'] = $request->description;
        }

        if(!empty($request->power)){
            $infos['power'] = $request->power;
        }

        if(!empty($request->category_id)){
            $infos['category_id'] = $request->category_id;
        }

        if(isset($request->status)){
            $infos['status'] = $request->status;
        }

        if(!empty($request->id)){
            $category = Category::find($request->id);
            $infos['slug'] = slug($category,'name');
            $category->update($infos);
        }else{
        	$category = new Category($infos);
        	$category->slug = slug($category,'name');
            $category->save();
        }

        return back()->with(['success'=>'เรียบร้อย']);
    }
}
