@props(['subcategories'=>$subcategories,'menu'=>$menu])
@if(!empty($subcategories))
    @foreach($subcategories->load(['subcategories'=>function($query){
            $query->where('status',1)->orderBy('power','desc');
        }]) as $y)
        <div class="d-flex justify-content-between">
            <div class="ms-2">
                - {{$y->name ?? ''}}
            </div>
            @if(!empty($menu))
            <div>
                <x-category::modal.category 
                    :category="$y"
                    id="categoryEdit{{$y->id}}" 
                    title="แก้ไข"
                />
                <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#categoryEdit{{$y->id}}">
                    <span class="text-nowrap"><i class="fas fa-edit"></i> แก้ไข</span>
                </a>

                <x-category::modal.category 
                    id="category{{$y->id}}" 
                    title="เพ่ิมหมวดหมู่ย่อย"
                    :parent="$y"
                />
                <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#category{{$y->id}}">
                    <span class="text-nowrap"><i class="fas fa-plus-circle"></i> เพิ่ม</span>
                </a>
            </div>
            @endif
        </div>
        @if(!empty($y->subcategories))
            <div class="ms-2">
                <x-category::subcategory 
                    :subcategories="$y->subcategories"
                    :menu="$menu"
                />
            </div>
        @endif
    @endforeach
@endif