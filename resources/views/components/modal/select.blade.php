@php 
  if(empty($slug)){
    $categories = \Pongsit\Category\Models\Category::where('status',1)->whereNull('category_id')->get(); 
  }else{
    $categories = \Pongsit\Category\Models\Category::where('status',1)->where('slug',$slug)->whereNull('category_id')->get()->first()->subcategories()->where('status',1)->get(); 
  }
@endphp

<div id="category_select" class="d-none">
  <div class="d-flex mb-2">
    <div style="width:80px;">{{$title}}</div>
    <div class="flex-fill">
      <select name="category_id" class="form-select category_selection" aria-label="Default select example">
        <option value="" selected>{{$defaultShow}}</option>
        @foreach($categories->load(['subcategories'=>function($query){
            $query->where('status',1)->orderBy('power','desc');
          }]) as $v)
          <option value="{{$v->id}}">{{$v->name}}</option>
          @if(empty($nosubcat))
            @if(!empty($v->subcategories))
              <x-category::modal.option
                :subcategories="$v->subcategories"
                :level="1"
              />
            @endif
          @endif
        @endforeach
        @if(request()->is('profile/category'))
          <option data-goto="{{route('category.index')}}">เพิ่มหมวดหมู่ใหม่</option>
        @endif
      </select>
    </div>
  </div>
</div>
@if(request()->is('profile/category'))
<script>
  $(function(){
    $('body').on('change','.category_selection', function() {
      var goto = $(this).find(":selected").data('goto');
      if(goto){
        window.location.replace(goto);
      }
    });
  });
</script>
@endif
