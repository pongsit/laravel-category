@props(['category'=>'','id'=>$id,'title'=>$title,'parent'=>''])

@once
  <x-category::modal.select 
    title="ภายใต้"
    defaultShow="หมวดหมู่หลัก"
  />
  <script>
    $(function(){
      var category_select = $('#category_select').html();
      $('.category_select').html(category_select);
    });
  </script>
@endonce

<div class="modal fade" id="{{$id}}" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{$title}}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('category.store')}}">
          @csrf
          @if(!empty($parent))
            <div class="d-flex mb-2">
              <div style="width:80px;">ภายใต้:</div>
              <div class="flex-fill">
                {{$parent->name}}
              </div>
              <input type="hidden" name="category_id" value="{{$parent->id}}">
            </div>
          @else
            <div class="category_select"></div>
          @endif
          <div class="d-flex mb-2">
            <div style="width:80px;">ชื่อ*:</div>
            <div class="flex-fill">
              <input type="text" name="name" class="form-control" placeholder="" value="{{$category->name ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">คำอธิบาย:</div>
            <div class="flex-fill">
              <input type="text" name="description" class="form-control" placeholder="" value="{{$category->description ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">power:</div>
            <div class="flex-fill">
              <input type="text" name="power" class="form-control" placeholder="" value="{{$category->power ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">สถานะ:</div>
            <div class="flex-fill">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" 
                  @if(!isset($category->status) || @$category->status == 1) checked @endif
                >
                <label class="form-check-label" for="inlineRadio1">เปิด</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" 
                  @if(isset($category->status) && @$category->status==0) checked @endif
                >
                <label class="form-check-label" for="inlineRadio2">ปิด</label>
              </div>
            </div>
          </div>
          @if(!empty($category->slug))
          <div class="d-flex mb-2">
            <div style="width:80px;">slug:</div>
            <div class="flex-fill">
              {{$category->slug ?? ''}}
            </div>
          </div>
          @endif
          @if(!empty($category->id))
            <input type="hidden" name="id" value="{{$category->id}}">
          @endif
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary {{$id}}-submit">ส่งข้อมูล</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(function () {
        $('body').on('click', '.{{$id}}-submit', function (e) {
            $('#{{$id}}').find('form').submit();
            $('#{{$id}}').modal('hide');
        });
    });
</script>