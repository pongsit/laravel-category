@extends('layouts.main')

@section('content')
<x-category::modal.category 
    id="insertMember" 
    title="เพิ่มรายการ" 
/>

<div class="container">
    <div class="row justify-content-center justify-content-md-start" data-masonry='{"percentPosition": true }'>
        <div class="col-12 col-md-6 col-lg-4 pb-3">
            <div class="card shadow">
                <div class="card-header pt-3 pb-3 d-flex justify-content-between">
                    <div><h6 class="m-0 font-weight-bold">หมวดหมู่</h6></div>
                    <div>
                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#insertMember">
                            <i class="fas fa-plus-circle"></i> เพิ่ม
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    @php
                        $bg = '';
                        $bg_mem = $bg; 
                        $empty = true; 
                    @endphp
                    @foreach($categories as $v)
                        <div class="d-flex justify-content-between" style="{{$bg}}">
                            <div>{{$v->name}}</div>
                        </div>
                        @php 
                            if(!empty($bg)){
                                $bg = $bg_mem;
                            }else{
                                $bg = 'background-color:rgb(33,37,41,0.6);';
                            }
                            $empty = false; 
                        @endphp
                    @endforeach
                    @if($empty)
                        กรุณาเพิ่มหมวดหมู่
                    @endempty
                </div>
            </div>
        </div>
        @foreach($categories->load(['subcategories'=>function($query){
                $query->where('status',1)->orderBy('power','desc');
            }]) as $v)
            <div class="col-12 col-md-6 col-lg-4 px-2 pb-3">
                <div class="card shadow">
                    <div class="card-header pt-3 pb-3 d-flex justify-content-between">
                        <div><h6 class="m-0 font-weight-bold">{{$v->name}}</h6></div>
                        <div class="text-right">
                            <x-category::modal.category 
                                :category="$v"
                                id="categoryEdit{{$v->id}}" 
                                title="แก้ไข"
                            />
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#categoryEdit{{$v->id}}">
                                <span class="text-nowrap"><i class="fas fa-edit"></i> แก้ไข</span>
                            </a>
                            <x-category::modal.category 
                                id="category{{$v->id}}" 
                                title="เพ่ิมหมวดหมู่ย่อย"
                                :parent="$v"
                            />
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#category{{$v->id}}">
                                <span class="text-nowrap"><i class="fas fa-plus-circle"></i> เพิ่ม</span>
                            </a>
                            {{-- <x:modal.syllabus 
                                :syllabus="$v" 
                                id="syllabus{{$v->id}}" 
                                title="แก้ไขวิชา"
                            />
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#syllabus{{$v->id}}">
                                <span class="text-nowrap"><i class="fas fa-edit"></i> แก้ไข</span>
                            </a>

                            <x:modal.subjects
                                :subjects="$subjects" 
                                :syllabus="$v" 
                                id="addsubject{{$v->id}}" 
                                title="เพิ่มวิชา"
                            />
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#addsubject{{$v->id}}">
                                <span class="text-nowrap"><i class="fas fa-plus-circle"></i> เพิ่มวิชา</span>
                            </a> --}}
                        </div>
                    </div>
                    <div class="card-body">
                        @if(count($v->subcategories)>0)
                            @php
                                $bg = '';
                                $bg_mem = $bg; 
                            @endphp
                            @foreach($v->subcategories->load(['subcategories'=>function($query){
                                    $query->where('status',1)->orderBy('power','desc');
                                }]) as $x)
                                <div class="d-flex justify-content-between" style="{{$bg}}">
                                    <div>
                                        {{$x->name ?? ''}}
                                    </div>
                                    <div>
                                        <x-category::modal.category 
                                            :category="$x"
                                            id="categoryEdit{{$x->id}}" 
                                            title="แก้ไข"
                                        />
                                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#categoryEdit{{$x->id}}">
                                            <span class="text-nowrap"><i class="fas fa-edit"></i> แก้ไข</span>
                                        </a>
                                        <x-category::modal.category 
                                            id="category{{$x->id}}" 
                                            title="เพ่ิมหมวดหมู่ย่อย"
                                            :parent="$x"
                                        />
                                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#category{{$x->id}}">
                                            <span class="text-nowrap"><i class="fas fa-plus-circle"></i> เพิ่ม</span>
                                        </a>
                                    </div>
                                </div>
                                @if(!empty($x->subcategories))
                                    <x-category::subcategory 
                                        :subcategories="$x->subcategories"
                                        :menu="true"
                                    />
                                @endif
                                @php 
                                    if(!empty($bg)){
                                        $bg = $bg_mem;
                                    }else{
                                        $bg = 'background-color:rgb(33,37,41,0.6);';
                                    }
                                @endphp
                            @endforeach
                        @else
                            ไม่มีหมวดหมู่ย่อย
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@stop